import { apiStatus } from '../../../lib/util';
import { Router } from 'express';

const Magento2Client = require('magento2-rest-client').Magento2Client;

module.exports = ({ config, db }) => {

  let mcApi = Router();

  mcApi.post('/contact', (req, res) => {

    let contactData = req.body
    if(!contactData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('contact', function (restClient) {
      var module = {};
      module.contact = function () {
        return restClient.post('/contact', contactData)
      }
      return module;
    })

    client.contact.contact().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err=> {
      apiStatus(res, err, 500);
    })
  })

  mcApi.get('/deliveryEstimation', (req, res) => {
    const client = Magento2Client(config.magento2.api);
    const sku = req.query.sku
    
    client.addMethods('estimation', (restClient) => {
      let module = {};
      module.estimation = function () {
        return restClient.get('/product/'+sku);
      };
      return module;
    });
    
    client.estimation
      .estimation()
      .then((result) => {
        apiStatus(res, result, 200);
      })
      .catch((err) => {
        apiStatus(res, err, 500);
      });
  });

  mcApi.get('/klarna', (req, res) => {
    const client = Magento2Client(config.magento2.api);
    const orderId = req.query.orderId
    client.addMethods('klarna', (restClient) => {
      let module = {};
      module.klarna = function () {
        return restClient.get('/klarna?orderId=' + orderId);
      };
      return module;
    });
    client.klarna
      .klarna()
      .then((result) => {
        apiStatus(res, result, 200);
      })
      .catch((err) => {
        apiStatus(res, err, 500);
      });
  });

  return mcApi
}
